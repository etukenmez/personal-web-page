export default defineNuxtConfig({
  modules:['@nuxtjs/color-mode'],
  app:{
    head:{
      htmlAttrs:{
        lang: 'en'
      },
      title: 'Erol Tukenmez',
      meta: [
        { name: 'description', content: 'Erol Tukenmez - Personal Web Site' }
      ],
      link : [
        { rel: "icon", type: "image/png", sizes:'16x16', href: "/icons/icon-16.png" },
        { rel: "icon", type: "image/png", sizes:'32x32', href: "/icons/icon-32.png" },
        { rel: "icon", type: "image/png", sizes:'96x96', href: "/icons/icon-96.png" },
        { rel: "icon", type: "image/png", sizes:'120x120', href: "/icons/icon-120.png" }
      ]
    }
  },
  devtools: { enabled: true },
  target:'static',
  css: ['~/assets/css/main.css'],
  
  colorMode: {
    classSuffix: ''
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  runtimeConfig: {
    FB_API_KEY: '',
    FB_AUTH_DOMAIN: '',
    FB_PROJECT_ID: '',
    FB_STORAGE_BUCKET: '', 
    FB_MESSAGING_SENDER_ID: '',
    FB_APP_ID: '',
  }
})
