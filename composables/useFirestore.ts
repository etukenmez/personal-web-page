import { FirebaseOptions, initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from "firebase/firestore";

export default function() {

    const connect = () => {
        const config = useRuntimeConfig()

        const firebaseConfig:FirebaseOptions = {
            apiKey: config.FB_API_KEY,
            authDomain: config.FB_AUTH_DOMAIN,
            projectId: config.FB_PROJECT_ID,
            storageBucket: config.FB_STORAGE_BUCKET,
            messagingSenderId: config.FB_MESSAGING_SENDER_ID,
            //appId: config.public.FB_APP_ID,
        }

        const app = initializeApp(firebaseConfig)

        return getFirestore(app)
    }

    const getSkills = async () => {
        const skills:Array<{id:string; title:string}> = []
        
        const response = await getDocs(collection(connect(),'skills'))
        
        response.forEach(collect => {
            skills.push({
                id:collect.id,
                title:collect.data().title
            })
        })
        
        return skills
    }

    return {
        getSkills
    }
}