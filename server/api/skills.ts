import useFirestore from "~/composables/useFirestore"

export default defineEventHandler(async (event) => {
    const { getSkills } = useFirestore()

    return await getSkills()
})